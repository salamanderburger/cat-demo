<!doctype html>

<?
include 'listcats.php';
?>

<html>
	<head>
		<title><?echo 'Cats CMS';?></title>
		<link href="css/bittsreset.css" rel="stylesheet" type="text/css">
		<!--reset-->
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
	</head>
	
	<body>
		<header>
			<h1>Cats Database Management System</h1>
		</header>
		<main>
			<p>Here is the main content of the system</p>
			<p>
				<?php 
					echo '<ul style="list-style-type:disc;">';
				?>
				<li>This is my 1 test item</li>
				<?php
				
					for($i=2;$i<=10;$i++){
						echo '<li>This is my '.$i.' test item</li>';		
					}
				?>
					
				<?php
					echo '</ul>';
				?>
			</p>
			<h3>Information about cats in my database</h3>
			
			<div id="listcats">
				<!-- print out a list  of cats -->
				<?
				listcats();
				
				?>
			</div>
			<div id="addinterface" >
				<h3>Adding a new cat Interface</h3>
				
					<div class="inprow">
						<label>Owner</label>
						<label id="catownerv" class="validator"></label>
						<input name="catowner_new" id="catowner_new">
						
					</div>
					<div  class="inprow">
						<label>Type</label>
						<input name="cattype_new" id="cattype_new" >
					</div>
					<div class="inprow">	
						<label>Weight</label>
						<input name="catweight_new" id="catweight_new">
					</div>
					<button onclick="addcat();">Add a new cat</button>
				
				
				
			</div>
			
		</main>
		<footer>
			<p>Here is the footer content</p>
			
			<script src="js/nano.js"></script>
			<script>
			function gid(d){
				
				return document.getElementById(d);	
			}
			
			function addcat(){
				
				var catowner = gid('catowner_new').value;
				var catowner_valid = gid('catownerv');
				
				//catowner_valid.innerHTML = "test";
				
				var cattype = gid('cattype_new').value;
				var catweight = gid('catweight_new').value;
				
				var valid = 1;
				
				if(catowner=="") valid = 0;
				
				
				if(!valid){
					catowner_valid.innerHTML = "Cat must have an owner";
					return false;	
				}
				
				
				//defining the ajax call object
				rq = new xmlHTTPRequestObject();
				
				//defining what kind of request (GET) and what values (catowner, cattype, catweight)
				rq.open("GET", "addcat.php?catowner_new="+catowner+"&cattype_new="+cattype+"&catweight_new="+catweight);
				
				//what to do when we do receive the request (readystate = 4)
				rq.onreadystatechange = function(){
					if (rq.readyState == 4){
						var res = rq.responseText;	
						document.getElementById('listcats').innerHTML = res;
					}
				}
				rq.send();
				
				//preventdefault submit
				
				
			}
			</script>
		</footer>
	</body>
	
</html>