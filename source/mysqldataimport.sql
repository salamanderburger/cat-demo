CREATE TABLE `cats` (
  `catid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `weight` decimal(10,2) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

#
# Dumping data for table `cats`
#

INSERT INTO `cats` VALUES (2, 'Simon', 'Siamese', '10.00');
INSERT INTO `cats` VALUES (3, 'Christine', 'Persian', '6.00');
INSERT INTO `cats` VALUES (4, 'Austin', 'British Blue', '11.00');
INSERT INTO `cats` VALUES (5, 'bibek', 'Kitty', '1.00');
INSERT INTO `cats` VALUES (7, 'azeb', 'bengal', '4.00');
INSERT INTO `cats` VALUES (15, 'Sean', 'Sabertooth', '300.32');
INSERT INTO `cats` VALUES (16, 'Sean', 'Sabertooth', '300.32');
INSERT INTO `cats` VALUES (17, 'Sean', 'Sabertooth', '300.32');
INSERT INTO `cats` VALUES (18, 'Sean', 'Sabertooth', '300.32');
INSERT INTO `cats` VALUES (19, 'Sean', 'Sabertooth', '300.32');